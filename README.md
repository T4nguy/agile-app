# TAA #

**BERNARD Tanguy**

### Objectif ###

L'objectif de ce tp est de développer un service REST pour un projet agile.

### Installation ###

1. Récupérer le projet
2. Ajouter dans ```/etc/hosts 127.0.0.1 myapp.taa.fr```
3. Exécuter ```docker-compose up``` (Lancer les conteneurs)

Les conteneurs lancés sont :

* La base de données mysql.
* Trois conteneurs contenant l'application REST avec lien à la base de données .
* Un nginx chargé du load balancing entre les différents serveurs rest. 

L'instance nginx est accessible via le port 80 à l'adresse myapp.taa.fr.

### Description ###
Application avec Spring Boot

* Controller : tout les controlleurs rest.
* Dao: requêtes en base de données.
* Domain:  toutes les entités JPA représentant le model du projet.
* Aspect: aspect de log, dès qu'une méthode d'un controleur est appelée.