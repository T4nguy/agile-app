package agile.controller;

import agile.dao.EpicDao;
import agile.dao.StoryDao;
import agile.domain.Epic;
import agile.domain.Story;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by tbernard on 10/11/15.
 */
@Controller
@RequestMapping("/story")
public class StoryController {



    @RequestMapping("/create-by-epic-id")
    @ResponseBody
    public String createByEpicId(int id, String name, int effort, int priority) {
        Story story = null;
        try {
            story = new Story(name, effort, priority);
            storyDao.createByEpicId(id,story);
        }
        catch (Exception ex) {
            return "Error creating the story: " + ex.toString();
        }
        return "Story succesfully created! (id = " + story.getId() + ")";
    }

    @RequestMapping("/create")
    @ResponseBody
    public String create(String name) {
        Story story = null;
        try {
            story = new Story();
            story.setName(name);
            storyDao.create(story);
        }
        catch (Exception ex) {
            return "Error creating the story: " + ex.toString();
        }
        return "Story succesfully created! (id = " + story.getId() + ")";
    }

    @RequestMapping("/assign-task")
    @ResponseBody
    public String assignTask(long idTask, long idStory) {

        storyDao.assignTaskToStory(idTask, idStory);
        return "Task succesfully assigned!";

    }

    @RequestMapping("/get-by-id")
    @ResponseBody
    public Story findById(int id) {

        Story story;
        try {

            story = storyDao.findById(id);
        }
        catch (Exception ex) {
            return null;
        }
        return story;

    }

    @RequestMapping("/all")
    @ResponseBody
    public List<Story> findAll() {

        return storyDao.findAll();

    }


    @RequestMapping("/get-by-epic-id")
    @ResponseBody
    public List<Story> findByEpicId(long id) {

        return storyDao.findStoriesByEpic(id);

    }

    @RequestMapping("/delete")
    @ResponseBody
    public String delete(long id) {
        try {

            storyDao.delete(id);
        }
        catch (Exception ex) {
            return "Error deleting the story:" + ex.toString();
        }
        return "Story succesfully deleted!";
    }



    @Autowired
    protected StoryDao storyDao;

}
