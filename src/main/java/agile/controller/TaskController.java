package agile.controller;

import agile.dao.StoryDao;
import agile.dao.TaskDao;
import agile.domain.Epic;
import agile.domain.Story;
import agile.domain.Task;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * Created by tbernard on 08/12/15.
 */
@Controller
@RequestMapping("/task")
public class TaskController {


    @RequestMapping("/all")
    @ResponseBody
    public List<Task> findAll() {

        return taskDao.findAll();

    }

    @RequestMapping("/create-by-story-id")
    @ResponseBody
    public String createByStoryId(int id, String name,String status) {
        Task task = null;
        try {
            task = new Task(name, status);
            taskDao.createByStoryId(id, task);
        }
        catch (Exception ex) {
            return "Error creating the task: " + ex.toString();
        }


        return "Task succesfully created! (id = " + task.getId() + ")";
    }

    @RequestMapping("/create")
    @ResponseBody
    public String create(String name) {
        Task task = null;
        try {
            task = new Task();
            task.setName(name);
            taskDao.create(task);
        }
        catch (Exception ex) {
            return "Error creating the task: " + ex.toString();
        }

        return "Task succesfully created! (id = " + task.getId() + ")";
    }


    @RequestMapping("/get-by-story-id")
    @ResponseBody
    public List<Task> findByEpicId(long id) {

        return taskDao.findTasksByStory(id);

    }

    @RequestMapping("/get")
    @ResponseBody
    public Task find(long id) {

        return taskDao.find(id);

    }

    @RequestMapping("/update")
    @ResponseBody
    public void update(long id, long user_id, String status) {

        taskDao.update(id, user_id, status);

    }

    @RequestMapping("/assign-user")
    @ResponseBody
    public String assignToTask(long idUser, long idTask) {

        taskDao.assignUserToTask(idUser,idTask);
        return "User succesfully assigned!";

    }

    @RequestMapping("/delete")
    @ResponseBody
    public String delete(long id) {
        try {
            taskDao.delete(id);
        }
        catch (Exception ex) {
            return "Error deleting the task:" + ex.toString();
        }
        return "Task succesfully deleted!";
    }



    @Autowired
    protected TaskDao taskDao;
}
