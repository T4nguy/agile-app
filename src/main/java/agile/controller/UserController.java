package agile.controller;

import agile.dao.TaskDao;
import agile.dao.UserDao;
import agile.domain.Epic;
import agile.domain.Task;
import agile.domain.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * Created by tbernard on 08/12/15.
 */
@Controller
@RequestMapping("/user")
public class UserController {

    @RequestMapping("/create")
    @ResponseBody
    public String create(String name) {
        User user = null;
        try {
            user = new User(name);
            userDao.create(user);
        }
        catch (Exception ex) {
            return "Error creating the user: " + ex.toString();
        }
        return "User succesfully created ! (id = " + user.getId() + ")";
    }


    @RequestMapping("/delete")
    @ResponseBody
    public String delete(long id) {
        try {
            userDao.delete(id);
        }
        catch (Exception ex) {
            return "Error deleting the user:" + ex.toString();
        }
        return "User succesfully deleted!";
    }



    @RequestMapping("/all")
    @ResponseBody
    public List<User> findAll() {

        return userDao.findAll();

    }



    @Autowired
    protected UserDao userDao;
}
