package agile.controller;

import agile.dao.EpicDao;
import agile.domain.Epic;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 *
 */
@Controller
@RequestMapping("/epic")
public class EpicController {

    // ------------------------
    // PUBLIC METHODS
    // ------------------------


    @RequestMapping("/create")
    @ResponseBody
    public String create(String name) {
        Epic epic = null;
        try {
            epic = new Epic(name);
            epicDao.create(epic);
        }
        catch (Exception ex) {
            return "Error creating the epic: " + ex.toString();
        }
        return "Epic succesfully created ! (id = " + epic.getId() + ")";
    }

    @RequestMapping("/get-by-id")
    @ResponseBody
    public String findById(int id) {

        Epic epic;
        try {

            epic = epicDao.findById(id);
        }
        catch (Exception ex) {
            return "Error to find the epic: " + ex.toString();
        }
        return "Epic: (name = " + epic.getName() + ")";

    }


    @RequestMapping("/assign-story")
    @ResponseBody
    public String assignStory(long idStory, long idEpic) {

        Epic epic = epicDao.assignStoryToEpic(idStory, idEpic);
        return "Story succesfully assigned!";

    }



    @RequestMapping("/all")
    @ResponseBody
    public List<Epic> findAll() {

        return epicDao.findAll();

    }

    @RequestMapping("/delete")
    @ResponseBody
    public String delete(long id) {
        try {
            Epic epic = new Epic(id);
            epicDao.delete(epic);
        }
        catch (Exception ex) {
            return "Error deleting the epic:" + ex.toString();
        }
        return "Epic succesfully deleted!";
    }


    // ------------------------
    // PRIVATE FIELDS
    // ------------------------

    @Autowired
    protected EpicDao epicDao;

} // class EpicController
