package agile.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.*;

@Entity
public class Story {
	
	private Long id;
	
	private Epic epic;
	
	private String name;
	
	private int effort;
	
	private int priority;
	
	private List<Task> tasks = new ArrayList<Task>();
	
	public Story(){
		
	}

	public Story(String name, int effort,int priority){
		this.name=name;
		this.effort=effort;
		this.priority=priority;

	}

	@JsonIgnore
	@ManyToOne(cascade=CascadeType.PERSIST)
	public Epic getEpic() {
		return epic;
	}

	public void setEpic(Epic epic) {
		this.epic = epic;
	}

	
	
	public int getEffort() {
		return effort;
	}

	public void setEffort(int effort) {
		this.effort = effort;
	}

	public int getPriority() {
		return priority;
	}

	public void setPriority(int priority) {
		this.priority = priority;
	}



	@Id
    @GeneratedValue
    public Long getId() {
        return id;
    }
	
	public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

	@OneToMany(cascade={CascadeType.PERSIST, CascadeType.REMOVE}, mappedBy="story", orphanRemoval=true, fetch= FetchType.EAGER)
	public List<Task> getTasks() {
		return tasks;
	}

	public void setTasks(List<Task> tasks) {
		this.tasks = tasks;
	}

	public void setId(Long id) {
		this.id = id;
	}


	public void removeTask(Task task) {
		if(this.tasks.contains(task)){
			this.tasks.remove(task);
		}
	}
	public void addTask(Task task) {
		this.tasks.add(task);
	}
}
