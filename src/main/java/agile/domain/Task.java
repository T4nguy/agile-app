package agile.domain;


import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.Null;
import java.util.List;
import java.util.Set;


@Entity
public class Task {
	
	private Long id;
	
	private String name;

	private String status="todo";
	
	private Story story;

    private Set<User> users;

	@JsonIgnore
	@ManyToOne (cascade=CascadeType.ALL)
	public Story getStory() {
		return story;
	}

	public void setStory(Story story) {
		this.story = story;
	}

	public Task(){
		
	}

	public Task(String name, String status){
		this.setName(name);
		if(status!= null){
			this.setStatus(status);
		}

	}
	
	public void setId(Long id) {
		this.id = id;
	}

    @ManyToMany(cascade=CascadeType.PERSIST)
    @JoinTable(name="user_task", joinColumns=@JoinColumn(name="task_id"), inverseJoinColumns=@JoinColumn(name="user_id"))
    public Set<User> getUsers() {
        return users;
    }

    public void setUsers(Set<User> users) {
        this.users = users;
    }



	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Id
    @GeneratedValue
    public Long getId() {
        return id;
    }
	
	public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void addUser(User user) {

        if(!this.users.contains(user)){
            this.users.add(user);
        }
		else{
			this.users.clear();
			this.users.add(user);

		}
    }
}
