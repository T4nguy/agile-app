package agile.domain;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Epic {

	private Long id;
	
	private String name;

	
	private List<Story> stories = new ArrayList<Story>();


	public Epic(){

	}

	public Epic(long id){
		this.id=id;

	}
	
	public Epic(String name){
		this.name=name;
	}
	
	@Id
    @GeneratedValue
    public Long getId() {
        return id;
    }
	public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

	
	@OneToMany(cascade={CascadeType.PERSIST, CascadeType.REMOVE}, mappedBy="epic", orphanRemoval=true, fetch=FetchType.EAGER)
	public List<Story> getStories() {
		return stories;
	}
	public void setStories(List<Story> stories) {
		this.stories = stories;
	}

	public void removeStory(Story story){
		if(this.stories.contains(story)){
			this.stories.remove(story);
		}
	}

	public void setId(Long id) {
		this.id = id;
	}


	public void addStory(Story story) {
		this.stories.add(story);
	}
}
