package agile.dao;

import agile.domain.Epic;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;


/**
 * Created by tbernard on 23/10/15.
 */
import agile.domain.Story;
import agile.domain.Task;
import agile.domain.User;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
@Transactional
public class UserDao {


    /**
     * Save the user in the database.
     */
    public void create(User user) {
        entityManager.persist(user);
        return;
    }




    public List<User> findAll() {
        List<User> users = new ArrayList<User>();
        try {
            users = entityManager.createQuery("SELECT d from User d", User.class).getResultList();

        } catch (Exception e) {
            System.out.println("Error");
            e.printStackTrace();
        }
        //tx.commit();
        entityManager.close();

        return users;
    }

    /**
     * Delete the user from the database.
     */
    public void delete(long user_id) {

        User user = entityManager.find(User.class,user_id);
        if (entityManager.contains(user)) {
            entityManager.remove(user);
        }

    }






    @PersistenceContext
    private EntityManager entityManager;
}
