package agile.dao;

import agile.domain.Epic;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;
import agile.domain.Story;
import agile.domain.Task;
import agile.domain.User;
import org.springframework.stereotype.Repository;
import java.util.ArrayList;
import java.util.List;

@Repository
@Transactional
public class TaskDao {

    /**
     * Save the epic in the database.
     */
    public void create(Task task) {
        entityManager.persist(task);
    }

    /**
            * Return the story having the passed id.
    */
    public Story findStoryById(long id) {
        String sql = "SELECT d FROM Story d WHERE d.id= :story_id";

        Query query = entityManager.createQuery(sql, Story.class);
        query.setParameter("story_id", id);


        return (Story) query.getSingleResult();
    }

    public void createByStoryId(int idStory, Task task) {
        Story story = findStoryById(idStory);
        task.setStory(story);
        entityManager.persist(task);
        return;
    }

    public List<Task> findAll() {
        List<Task> tasks = new ArrayList<Task>();
        try {
            tasks = entityManager.createQuery("SELECT d from Task d", Task.class).getResultList();

        } catch (Exception e) {
            System.out.println("Error");
            e.printStackTrace();
        }
        //tx.commit();
        entityManager.close();

        return tasks;
    }

    /**
     * List of tasks by id story
     * @param story_id id of Story
     * @return
     */
    public List<Task> findTasksByStory(long story_id) {
        // type long otherwise exception
        Story myStory = entityManager.find(Story.class, story_id);

        String sql = "SELECT e FROM Task e WHERE e.story= :myStory";


        Query query = entityManager.createQuery(sql, Task.class);
        query.setParameter("myStory", myStory);


        return query.getResultList();

    }


    /**
     * Delete the user from the database.
     */
    public void delete(long task_id) {

        Task task = entityManager.find(Task.class,task_id);
        Story story = entityManager.find(Story.class, task.getStory().getId());
        story.removeTask(task);
        entityManager.merge(story);
        task.setStory(null);
        entityManager.merge(task);
        entityManager.remove(task);


    }

    public void assignUserToTask(long user_id, long task_id) {

        User user = entityManager.find(User.class, user_id);
        Task task = entityManager.find(Task.class, task_id);

        task.addUser(user);
        entityManager.merge(task);

    }



    @PersistenceContext
    private EntityManager entityManager;


    public Task find(long id) {
        return entityManager.find(Task.class, id);
    }

    public void update(long id, long user_id, String status) {
        User user = entityManager.find(User.class, user_id);
        Task task = entityManager.find(Task.class, id);

        task.addUser(user);
        task.setStatus(status);
        entityManager.merge(task);

    }
}