package agile.dao;

import agile.domain.Epic;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;


/**
 * Created by tbernard on 23/10/15.
 */
import agile.domain.Story;
import agile.domain.Task;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is used to access data for the User entity.
 * Repository annotation allows the component scanning support to find and
 * configure the DAO wihtout any XML configuration and also provide the Spring
 * exceptiom translation.
 * Since we've setup setPackagesToScan and transaction manager on
 * DatabaseConfig, any bean method annotated with Transactional will cause
 * Spring to magically call begin() and commit() at the start/end of the
 * method. If exception occurs it will also call rollback().
 */
@Repository
@Transactional
public class EpicDao {

    // ------------------------
    // PUBLIC METHODS
    // ------------------------

    /**
     * Save the epic in the database.
     */
    public void create(Epic epic) {
        entityManager.persist(epic);
        return;
    }

    /**
     * Delete the epic from the database.
     */
    public void delete(Epic epic) {
        if (entityManager.contains(epic))
            entityManager.remove(epic);
        else
            entityManager.remove(entityManager.merge(epic));
        return;
    }




    /**
     * Return the epic having the passed id.
     */
    public Epic findById(long id) {
        //return (Epic) entityManager.createQuery("SELECT d FROM Epic d WHERE d.id="+id).getSingleResult();
        return entityManager.find(Epic.class, id);
    }


    public List<Epic> findAll() {
        List<Epic> epics = new ArrayList<Epic>();
        try {
            epics = entityManager.createQuery("SELECT d from Epic d", Epic.class).getResultList();

        } catch (Exception e) {
            System.out.println("Error");
            e.printStackTrace();
        }
        //tx.commit();
        entityManager.close();


        return epics;
    }

    public Epic assignStoryToEpic(long idStory, long idEpic) {

        Story story = entityManager.find(Story.class,idStory);
        Epic epic = entityManager.find(Epic.class,idEpic);

        epic.addStory(story);
        story.setEpic(epic);
        entityManager.merge(epic);
        entityManager.merge(story);

        return epic;

    }

    /**
     * Update the passed user in the database.
     */
    public void update(Epic user) {
        entityManager.merge(user);
        return;
    }

    // ------------------------
    // PRIVATE FIELDS
    // ------------------------

    // An EntityManager will be automatically injected from entityManagerFactory
    // setup on DatabaseConfig class.
    @PersistenceContext
    private EntityManager entityManager;


} // class EpicDao

