package agile.dao;

import agile.domain.Epic;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;


/**
 * Created by tbernard on 23/10/15.
 */
import agile.domain.Story;
import agile.domain.Task;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is used to access data for the User entity.
 * Repository annotation allows the component scanning support to find and
 * configure the DAO wihtout any XML configuration and also provide the Spring
 * exceptiom translation.
 * Since we've setup setPackagesToScan and transaction manager on
 * DatabaseConfig, any bean method annotated with Transactional will cause
 * Spring to magically call begin() and commit() at the start/end of the
 * method. If exception occurs it will also call rollback().
 */
@Repository
@Transactional
public class StoryDao {

    // ------------------------
    // PUBLIC METHODS
    // ------------------------

    /**
     * Save the story in the database.
     */
    public void create(Story story) {
        entityManager.persist(story);
        return;
    }

    public void createByEpicId(int idEpic, Story story) {
        Epic epic = findEpicById(idEpic);
        story.setEpic(epic);
        entityManager.persist(story);
        return;
    }


    /**
     * Delete the story from the database.
     */
    public void delete(long id) {

        Story story = entityManager.find(Story.class,id);
        Epic epic = entityManager.find(Epic.class,story.getEpic().getId());
        epic.removeStory(story);
        entityManager.merge(epic);
        story.setEpic(null);
        entityManager.merge(story);
        entityManager.remove(story);
    }




    /**
     * Return the epic having the passed id.
     */
    public Epic findEpicById(long id) {
        String sql = "SELECT d FROM Epic d WHERE d.id= :epic_id";

        Query query = entityManager.createQuery(sql, Epic.class);
        query.setParameter("epic_id", id);


        return (Epic) query.getSingleResult();
    }


    public List<Story> findAll() {
        List<Story> stories = new ArrayList<Story>();
        try {
            stories = entityManager.createQuery("SELECT d from Story d", Story.class).getResultList();

        } catch (Exception e) {
            System.out.println("Error");
            e.printStackTrace();
        }
        //tx.commit();
        entityManager.close();

        return stories;
    }


    /**
     * Return the user having the passed id.
     */
    public Story findById(long id) {
        //return (Epic) entityManager.createQuery("SELECT d FROM Epic d WHERE d.id="+id).getSingleResult();
        return entityManager.find(Story.class, id);
    }


    public List<Story> findStoriesByEpic(long epic_id){

        // type long otherwise exception
        Epic myEpic = entityManager.find(Epic.class, epic_id);

        String sql = "SELECT e FROM Story e WHERE e.epic= :myEpic";


        Query query = entityManager.createQuery(sql, Story.class);
        query.setParameter("myEpic", myEpic);


        return query.getResultList();
    }

    /**
     * Update the passed story in the database.
     */
    public void update(Story story) {
        entityManager.merge(story);
        return;
    }

    public void assignTaskToStory(long idTask, long idStory) {
        Story story = entityManager.find(Story.class, idStory);
        Task task = entityManager.find(Task.class, idTask);

        story.addTask(task);
        task.setStory(story);
        entityManager.merge(story);
        entityManager.merge(task);



    }

    // ------------------------
    // PRIVATE FIELDS
    // ------------------------

    // An EntityManager will be automatically injected from entityManagerFactory
    // setup on DatabaseConfig class.
    @PersistenceContext
    private EntityManager entityManager;


} // class UserDao

