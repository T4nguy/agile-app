#!/bin/bash
docker rm -f $(docker ps -aq)
docker build -t my-maven3 . 
docker run --name demo-mysql -e MYSQL_ROOT_PASSWORD=root -e MYSQL_DATABASE=testdb -e MYSQL_USER=user -e MYSQL_PASSWORD=root -d mysql:5.6
docker run -p 8080:8080 --name demo-app --link demo-mysql:mysql -d my-maven3
